-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local vicious = require("vicious")
local scratch = require("scratch")
local lain = require("lain")
--local revelation=require("revelation")
xdg_menu = require("archmenu")

function run_once(prg)
    awful.util.spawn_with_shell("pgrep -u $USER -x " .. prg .. " || (" .. prg .. ")")
end

run_once("parcellite")
run_once("nitrogen --restore")
run_once("numlockx on")
run_once("unclutter -idle 10")


-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init("/home/matija/.config/awesome/themes/default/theme.lua")

--revelation.init()
-- This is used later as the default terminal and editor to run.
terminal = "urxvtc"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor
exec = awful.util.spawn

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod1"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
    awful.layout.suit.tile,
    awful.layout.suit.floating,
    lain.layout.uselesstile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier
}
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {
	names  = { "term", "www", "IM", "text", "fun", "FM"},
	layout = { layouts[3], layouts[3], layouts[2], layouts[3], layouts[2], layouts[2]}
	}
for s = 1, screen.count() do
 -- Each screen has its own tag table.
      tags[s] = awful.tag(tags.names, s, tags.layout)
end
-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu
myawesomemenu = {
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", awesome.quit }
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
				{ "Applications", xdgmenu },
				{ "FM", "dbus-launch pcmanfm" },
				{ "----------"},
				{ "Shutdown", "poweroff"},
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox

-- Create a textclock widget
mytextclock = awful.widget.textclock()

markup = lain.util.markup
white  = beautiful.fg_focus
gray   = "#858585"

-- Battery
batwidget = lain.widgets.bat({
    settings = function()
        bat_perc = bat_now.perc
        if bat_perc == "N/A" then bat_perc = "Plug" end
        widget:set_markup(markup(gray, " Bat ") .. bat_perc .. "")
    end
})
    batwidget:buttons(awful.util.table.join(
    awful.button({ }, 1, function () exec("urxvt -e sudo powertop") end)
    ))

-- Net checker
netwidget = lain.widgets.net({
    settings = function()
        if net_now.state == "up" then net_state = "On"
        else net_state = "Off" end
        widget:set_markup(markup(gray, " Net ") .. net_state .. " ")
    end
})
    netwidget:buttons(awful.util.table.join(
    awful.button({ }, 1, function () exec("urxvt -e sudo iptraf-ng -i all") end),
    awful.button({ }, 3, function () exec("urxvt -e nbwmon") end)
    ))

-- CPU
cpuwidget = lain.widgets.sysload({
    settings = function()
        widget:set_markup(markup(gray, " Cpu ") .. load_1 .. "")
    end
})
    cpuwidget:buttons(awful.util.table.join(
    awful.button({ }, 1, function () exec("urxvt -e htop --sort-key=PERCENT_CPU") end)
    ))

-- MEM
memwidget = lain.widgets.mem({
    settings = function()
        widget:set_markup(markup(gray, " Mem ") .. mem_now.used .. " / " .. mem_now.total .. "")
    end
})
    memwidget:buttons(awful.util.table.join(
    awful.button({ }, 1, function () exec("urxvt -e htop --sort-key=PERCENT_MEM") end)
    ))

-- MPD
    mympdwidget =  wibox.widget.textbox()
        --mympdwidget.width = 140
	vicious.register(mympdwidget, vicious.widgets.mpd, '<span color="grey"> Mpd </span>(<span color="#CC9393">${state}</span>): ${Artist} - ${Title}', 5)
	mympdwidget:buttons(awful.util.table.join(
	                    awful.button({ }, 1, function () awful.util.spawn("mpc toggle", false) end),
	                    awful.button({ }, 2, function () awful.util.spawn("urxvt -e ncmpcpp", true) end),
	                    awful.button({ }, 4, function () awful.util.spawn("mpc next", false) end),
	                    awful.button({ }, 5, function () awful.util.spawn("mpc prev", false) end)
			))

-- Calendar
lain.widgets.calendar:attach(mytextclock, { fg = beautiful.fg_focus })


-- Volumewidget
volwidget = wibox.widget.textbox()
vicious.register(volwidget, vicious.widgets.volume, '<span color="grey"> Vol</span> $1', 2, "Master")
    -- Keybindings for widget
    volwidget:buttons(awful.util.table.join(
    awful.button({ }, 1, function () exec("urxvt -e alsamixer") end),
    awful.button({ }, 2, function () exec("amixer -q sset Master toggle")   end),
    awful.button({ }, 4, function () exec("/home/matija/bin/vol.sh up", false) end),
    awful.button({ }, 5, function () exec("/home/matija/bin/vol.sh down", false) end)
    ))

-- Coretemp
tempwidget = lain.widgets.temp({
    settings = function()
        widget:set_markup(markup(gray, " Temp ") .. coretemp_now .. "")
    end
})
tempwidget:buttons(awful.util.table.join(
awful.button({ }, 1, function () exec("urxvt -e watch sensors") end)
))


    spacer = wibox.widget.textbox()
    spacer:set_markup(" |" )

  --  batwidget = wibox.widget.textbox()
  --  vicious.register(batwidget, vicious.widgets.bat,  '<span color="white"> BAT:</span> $1$2%', 67, "BAT1")

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)


    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(spacer)
    right_layout:add(memwidget)
    right_layout:add(spacer)
    right_layout:add(cpuwidget)
    right_layout:add(spacer)
    right_layout:add(netwidget)
    right_layout:add(spacer)
    right_layout:add(tempwidget)
    right_layout:add(spacer)
    right_layout:add(mympdwidget)
    right_layout:add(spacer)
    right_layout:add(batwidget)
    right_layout:add(spacer)
    right_layout:add(volwidget)
    right_layout:add(spacer)
    if s == 1 then right_layout:add(wibox.widget.systray()) end
    right_layout:add(mytextclock)
    right_layout:add(mylayoutbox[s])

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
--    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "a",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "s",  awful.tag.viewnext       ),
--    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", 	function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", 	function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", 	function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", 	function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", 	awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "#49", 	function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", 	awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", 	awesome.quit),
    awful.key({ modkey,           }, "c",       function () lain.widgets.calendar:show(7) end),

    awful.key({ modkey,           }, "l",     	function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     	function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     	function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     	function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     	function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     	function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", 	function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", 	function () awful.layout.inc(layouts, -1) end),
    awful.key({ modkey 		  }, "i",     	function () scratch.drop("urxvt", "center", nil, 0.60, 0.30) end),
    awful.key({ modkey 		  }, "o",     	function () scratch.drop("leafpad", "bottom", nil, nil, 0.30) end),
    awful.key({ modkey,           }, "F2",    	function () awful.util.spawn("google-chrome-unstable")                          end),
    awful.key({ modkey,           }, "F1",    	function () awful.util.spawn("firefox")                          end),
    awful.key({ modkey,           }, "F4",    	function () awful.util.spawn("qupzilla")                          end),
    awful.key({                   }, "#148",    function () awful.util.spawn("speedcrunch")                          end),
    awful.key({                   }, "#121",    function () awful.util.spawn("amixer -q sset Master toggle")                          end),
    awful.key({                   }, "#122",    function () awful.util.spawn("/home/matija/bin/vol.sh down")                          end),
    awful.key({                   }, "#123",    function () awful.util.spawn("/home/matija/bin/vol.sh up")                          end),
    awful.key({                   }, "#173",    function () awful.util.spawn("mpc prev")                          end),
    awful.key({                   }, "#174",    function () awful.util.spawn("mpc stop")                          end),
    awful.key({                   }, "#171",    function () awful.util.spawn("mpc next")                          end),
    awful.key({ modkey,           }, "F3",      function () awful.util.spawn("dwb")                     end),
    awful.key({ "Mod1", "Control" }, "l",       function () awful.util.spawn("i3lock")                  end),
--    awful.key({ modkey		  }, "q", revelation),
    awful.key({ modkey, "Shift"   }, "#49", 	function () awful.util.spawn(terminal .. " -name mail -geometry 200x55 -e mutt") end),

    awful.key({ modkey, "Control" }, "+", function () lain.util.useless_gaps_resize(1) end),
    awful.key({ modkey, "Control" }, "-", function () lain.util.useless_gaps_resize(-1) end),
    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Prompt
    awful.key({ modkey },            "r",     	function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey,           }, "Escape", function (c) c:kill() 	                     end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.movetotag(tag)
                          end
                     end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.toggletag(tag)
                          end
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     keys = clientkeys,
                     buttons = clientbuttons } },
	  { rule = { class = "MPlayer" },
      properties = { floating = true } },
	  { rule = { class = "mpv" },
      properties = { floating = true } },
	  { rule = { class = "mpv" },
      properties = { floating = true } },
	  { rule = { class = "pinentry" },
      properties = { floating = true } },
	  { rule = { class = "gimp" },
      properties = { floating = true } },
	  { rule = { class = "URxvt" },
      properties = { floating = false, tag = tags[1] [1] } },
    	  { rule = { instance = "mail" },
      properties = { floating = true, tag = tags[1] [3] } },
          { rule = { class = "Firefox" },
      properties = { tag = tags[1] [2] } },
          { rule = { class = "QupZilla" },
      properties = { tag = tags[1] [2] } },
          { rule = { class = "Chromium" },
      properties = { tag = tags[1] [5] } },
          { rule = { class = "Google-chrome-unstable" },
      properties = { tag = tags[1] [5] } },
          { rule = { class = "com-sun-javaws-Main" },
      properties = { floating = true, tag = tags[1] [5], switchtotag = true } },
          { rule = { class = "Pcmanfm" },
      properties = { floating = true, tag = tags[1] [6] } },
          { rule = { class = "Geany" },
      properties = { floating = false, tag = tags[1] [4] } },
          { rule = { class = "Epdfview" },
      properties = { floating = false, tag = tags[1] [4] } },
      	  { rule =  { class = "Pidgin" },
      properties = { floating = true, tag = tags[2] [3] } },
          { rule = { class = "Vlc" },
      properties = { floating = true, tag = tags[1] [5], switchtotag = true } },
          { rule = { class = "Gvim" },
      properties = { floating = false, tag = tags[1] [4] } },
          { rule = { class = "Claws-mail" },
      properties = { floating = true, tag = tags[1] [7] } },
          { rule = { class = "Thunderbird" },
      properties = { floating = false, tag = tags[1] [7] } },
          { rule = { class = "Xephyr" },
      properties = { floating = true, tag = tags[1] [10] } },
          { rule = { class = "Speedcrunch" },
      properties = { floating = true} },
          { rule = { class = "Orage" },
      properties = { floating = true} },
          { rule = { class = "Wine" },
      properties = { floating = true, tag = tags[1] [4] } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align("center")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}