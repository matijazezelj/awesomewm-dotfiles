 local menu98edb85b00d9527ad5acebe451b3fae6 = {
     {"7-Zip FM", "7zFM", "/usr/share/icons/hicolor/32x32/apps/p7zip.png" },
     {"Akonaditray", "akonaditray", "/usr/share/icons/hicolor/22x22/apps/akonaditray.png" },
     {"Ark", "ark -caption Ark ", "/usr/share/icons/hicolor/16x16/apps/ark.png" },
     {"KCalc", "kcalc -caption KCalc"},
     {"KMail Import Wizard", "importwizard", "/usr/share/icons/hicolor/64x64/apps/kontact-import-wizard.png" },
     {"KNotes", "knotes", "/usr/share/icons/hicolor/16x16/apps/knotes.png" },
     {"KWrite", "kwrite "},
     {"Kate", "kate -b ", "/usr/share/icons/hicolor/16x16/apps/kate.png" },
     {"LXImage", "lximage-qt", "/usr/share/icons/hicolor/48x48/apps/lximage-qt.png" },
     {"Leafpad", "leafpad ", "/usr/share/icons/hicolor/16x16/apps/leafpad.png" },
     {"PCManFM File Manager", "pcmanfm-qt "},
     {"Root Terminal", "gksu -l gnome-terminal", "/usr/share/pixmaps/gksu-root-terminal.png" },
     {"Screenshot", "lximage-qt --screenshot"},
     {"SystemDX", "ssx systemdx", "/usr/share/pixmaps/systemdx.png" },
     {"SystemDX - Tracking journal in real-time", "ssx systemdx -tj", "/usr/share/pixmaps/systemdx.png" },
     {"SystemDX - Udev Monitor", "ssx systemdx -u", "/usr/share/pixmaps/systemdx.png" },
     {"Vi IMproved", "gvim -f ", "/usr/share/pixmaps/gvim.png" },
 }

 local menude7a22a0c94aa64ba2449e520aa20c99 = {
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
     {"Marble", "marble-mobile ", "/usr/share/icons/hicolor/16x16/apps/marble.png" },
     {"Marble", "marble-qt ", "/usr/share/icons/hicolor/16x16/apps/marble.png" },
     {"Marble", "marble-touch ", "/usr/share/icons/hicolor/16x16/apps/marble.png" },
     {"Marble", "marble ", "/usr/share/icons/hicolor/16x16/apps/marble.png" },
 }

 local menu251bd8143891238ecedc306508e29017 = {
     {"Awesomenauts", "steam steam://rungameid/204300"},
     {"Dosbox", "dosbox", "/usr/share/pixmaps/dosbox.png" },
     {"Dota 2", "steam steam://rungameid/570", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Fallout", "/usr/share/playonlinux/playonlinux --run \"Fallout\" ", "///home/matija/.PlayOnLinux//icones/full_size/Fallout" },
     {"Fallout 2", "/usr/share/playonlinux/playonlinux --run \"Fallout 2\" ", "///home/matija/.PlayOnLinux//icones/full_size/Fallout 2" },
     {"Freeciv", "freeciv-gtk2", "/usr/share/icons/hicolor/16x16/apps/freeciv-client.png" },
     {"Freeciv modpack installer", "freeciv-modpack", "/usr/share/icons/hicolor/16x16/apps/freeciv-modpack.png" },
     {"Freeciv server", "xterm -e freeciv-server", "/usr/share/icons/hicolor/16x16/apps/freeciv-server.png" },
     {"Gunpoint", "steam steam://rungameid/206190", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Hammerwatch", "steam steam://rungameid/239070"},
     {"Icewind Dale", "/usr/share/playonlinux/playonlinux --run \"Icewind Dale\" ", "///home/matija/.PlayOnLinux//icones/full_size/Icewind Dale" },
     {"Icewind Dale II", "/usr/share/playonlinux/playonlinux --run \"Icewind Dale II\" ", "///home/matija/.PlayOnLinux//icones/full_size/Icewind Dale II" },
     {"Left 4 Dead 2", "steam steam://rungameid/550", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"PlayOnLinux", "playonlinux", "///usr/share/playonlinux/etc/playonlinux.png" },
     {"Steam", "/usr/bin/steam ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Torchlight", "/usr/share/playonlinux/playonlinux --run \"Torchlight\" ", "///home/matija/.PlayOnLinux//icones/full_size/Torchlight" },
 }

 local menud334dfcea59127bedfcdbe0a3ee7f494 = {
     {"E-book Viewer", "ebook-viewer --detach ", "/usr/share/pixmaps/calibre-viewer.png" },
     {"GNU Image Manipulation Program", "gimp-2.8 ", "/usr/share/icons/hicolor/16x16/apps/gimp.png" },
     {"Gwenview", "gwenview  -caption Gwenview ", "/usr/share/icons/hicolor/16x16/apps/gwenview.png" },
     {"Image Viewer", "gpicview ", "/usr/share/icons/hicolor/48x48/apps/gpicview.png" },
     {"KSnapshot", "ksnapshot -caption KSnapshot", "/usr/share/icons/hicolor/16x16/apps/ksnapshot.png" },
     {"LRF Viewer", "lrfviewer ", "/usr/share/pixmaps/calibre-viewer.png" },
     {"LXImage", "lximage-qt", "/usr/share/icons/hicolor/48x48/apps/lximage-qt.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
     {"Okular", "okular   -caption Okular", "/usr/share/icons/hicolor/16x16/apps/okular.png" },
     {"Screenshot", "lximage-qt --screenshot"},
     {"digiKam", "digikam -caption digiKam ", "/usr/share/icons/hicolor/16x16/apps/digikam.png" },
     {"feh", "feh ", "///usr/share/feh/images/feh.png" },
     {"showFoto", "showfoto  -caption showFoto ", "/usr/share/icons/hicolor/16x16/apps/showfoto.png" },
 }

 local menuc8205c7636e728d448c2774e6a4a944b = {
     {"Avahi SSH Server Browser", "/usr/bin/bssh"},
     {"Avahi VNC Server Browser", "/usr/bin/bvnc"},
     {"Claws Mail", "claws-mail ", "/usr/share/icons/hicolor/48x48/apps/claws-mail.png" },
     {"Cloud Storage Manager", "storageservicemanager", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"Contact Theme Editor", "contactthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kaddressbook.png" },
     {"Copy", "CopyAgent", "/usr/share/pixmaps/copy-agent.png" },
     {"Dropbox", "dropboxd", "/usr/share/pixmaps/dropbox.png" },
     {"FileZilla", "filezilla", "/usr/share/icons/hicolor/16x16/apps/filezilla.png" },
     {"Firefox", "/usr/lib/firefox/firefox ", "/usr/share/icons/hicolor/16x16/apps/firefox.png" },
     {"Firefox32", "bin32-firefox ", "/usr/share/pixmaps/firefox32.png" },
     {"Firefox32 - Safe Mode", "bin32-firefox -safe-mode ", "/usr/share/pixmaps/firefox32.png" },
     {"Google Chrome", "/usr/bin/google-chrome-unstable "},
     {"Google Chrome (unstable)", "/usr/bin/google-chrome-unstable ", "/usr/share/icons/hicolor/16x16/apps/google-chrome-unstable.png" },
     {"KMail", "kmail -caption KMail ", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"KMail Header Theme Editor", "headerthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"Konqueror", "kfmclient openProfile webbrowsing", "/usr/share/icons/hicolor/16x16/apps/konqueror.png" },
     {"Opera", "/usr/bin/opera ", "/usr/share/icons/hicolor/16x16/apps/opera-browser.png" },
     {"Pidgin Internet Messenger", "pidgin", "/usr/share/icons/hicolor/16x16/apps/pidgin.png" },
     {"Popcorn Time", "popcorntime ", "/usr/share/pixmaps/popcorntime.png" },
     {"Qtransmission Bittorrent Client", "transmission-qt ", "/usr/share/pixmaps/transmission-qt.png" },
     {"Skype", "skype ", "/usr/share/icons/hicolor/16x16/apps/skype.png" },
     {"Steam", "/usr/bin/steam ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Sylpheed", "sylpheed", "/usr/share/pixmaps/sylpheed.png" },
     {"Thunderbird", "thunderbird ", "/usr/share/icons/hicolor/16x16/apps/thunderbird.png" },
     {"Transmission", "transmission-gtk ", "/usr/share/icons/hicolor/16x16/apps/transmission.png" },
     {"Zenmap", "zenmap ", "///usr/share/zenmap/pixmaps/zenmap.png" },
     {"Zenmap (as root)", "/usr/share/zenmap/su-to-zenmap.sh ", "///usr/share/zenmap/pixmaps/zenmap.png" },
     {"dwb", "dwb ", "/usr/share/pixmaps/dwb.png" },
 }

 local menudf814135652a5a308fea15bff37ea284 = {
     {"Calibre", "calibre --detach ", "/usr/share/pixmaps/calibre-gui.png" },
     {"Claws Mail", "claws-mail ", "/usr/share/icons/hicolor/48x48/apps/claws-mail.png" },
     {"Cloud Storage Manager", "storageservicemanager", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"Contact Theme Editor", "contactthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kaddressbook.png" },
     {"Edit E-book", "ebook-edit --detach ", "/usr/share/pixmaps/calibre-ebook-edit.png" },
     {"Gummi", "gummi ", "/usr/share/pixmaps/gummi.png" },
     {"KAddressBook", "kaddressbook ", "/usr/share/icons/hicolor/16x16/apps/kaddressbook.png" },
     {"KMail", "kmail -caption KMail ", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"KMail Header Theme Editor", "headerthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"KOrganizer", "korganizer ", "/usr/share/icons/hicolor/16x16/apps/korganizer.png" },
     {"Kontact", "kontact", "/usr/share/icons/hicolor/16x16/apps/kontact.png" },
     {"LibreOffice", "libreoffice ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-startcenter.png" },
     {"LibreOffice Base", "libreoffice --base ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-base.png" },
     {"LibreOffice Calc", "libreoffice --calc ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-calc.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
     {"LibreOffice Impress", "libreoffice --impress ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-impress.png" },
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
     {"LibreOffice Writer", "libreoffice --writer ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-writer.png" },
     {"Okular", "okular   -caption Okular", "/usr/share/icons/hicolor/16x16/apps/okular.png" },
     {"Zathura", "zathura "},
     {"ePDFViewer", "epdfview ", "/usr/share/icons/hicolor/24x24/apps/epdfview.png" },
 }

 local menue6f43c40ab1c07cd29e4e83e4ef6bf85 = {
     {"CMake", "cmake-gui ", "/usr/share/pixmaps/CMakeSetup32.png" },
     {"Geany", "geany ", "/usr/share/icons/hicolor/16x16/apps/geany.png" },
     {"Kompare", "kompare -caption Kompare -o ", "/usr/share/icons/hicolor/16x16/apps/kompare.png" },
     {"Meld", "meld ", "/usr/share/icons/hicolor/16x16/apps/meld.png" },
     {"Qt Assistant", "/usr/lib/qt/bin/assistant", "/usr/share/icons/hicolor/32x32/apps/assistant.png" },
     {"Qt Designer", "/usr/lib/qt/bin/designer", "/usr/share/icons/hicolor/128x128/apps/designer.png" },
     {"Qt Linguist", "/usr/lib/qt/bin/linguist", "/usr/share/icons/hicolor/16x16/apps/linguist.png" },
     {"Qt QDbusViewer ", "/usr/lib/qt/bin/qdbusviewer", "/usr/share/icons/hicolor/32x32/apps/qdbusviewer.png" },
     {"Qt4 Assistant ", "assistant-qt4", "/usr/share/icons/hicolor/32x32/apps/assistant-qt4.png" },
     {"Qt4 Designer", "designer-qt4", "/usr/share/icons/hicolor/128x128/apps/designer-qt4.png" },
     {"Qt4 Linguist ", "linguist-qt4", "/usr/share/icons/hicolor/16x16/apps/linguist-qt4.png" },
     {"Qt4 QDbusViewer ", "qdbusviewer-qt4", "/usr/share/icons/hicolor/32x32/apps/qdbusviewer-qt4.png" },
     {"Sublime Text 3 Dev", "subl3 ", "/usr/share/icons/hicolor/16x16/apps/sublime-text.png" },
 }

 local menu52dd1c847264a75f400961bfb4d1c849 = {
     {"Cantata", "cantata ", "/usr/share/icons/hicolor/16x16/apps/cantata.png" },
     {"Clementine", "clementine ", "/usr/share/icons/hicolor/64x64/apps/application-x-clementine.png" },
     {"EasyTAG", "easytag ", "/usr/share/icons/hicolor/16x16/apps/easytag.png" },
     {"Plex Home Theater", "/usr/bin/plexhometheater.sh", "///usr/share/pixmaps/plexhometheater.png" },
     {"Qt V4L2 test Utility", "qv4l2", "/usr/share/icons/hicolor/16x16/apps/qv4l2.png" },
     {"QtMPC", "QtMPC"},
     {"Sonata", "sonata", "/usr/share/pixmaps/sonata.png" },
     {"VLC media player", "/usr/bin/vlc --started-from-file ", "/usr/share/icons/hicolor/16x16/apps/vlc.png" },
     {"XBMC Media Center", "xbmc", "/usr/share/icons/hicolor/48x48/apps/xbmc.png" },
 }

 local menuee69799670a33f75d45c57d1d1cd0ab3 = {
     {"Avahi Zeroconf Browser", "/usr/bin/avahi-discover"},
     {"Dolphin", "dolphin  -caption Dolphin "},
     {"File Manager PCManFM", "pcmanfm "},
     {"GParted", "/usr/bin/gparted_polkit ", "/usr/share/icons/hicolor/16x16/apps/gparted.png" },
     {"Htop", "xterm -e htop", "/usr/share/pixmaps/htop.png" },
     {"KSysGuard", "ksysguard "},
     {"Konsole", "konsole"},
     {"Nepomuk Backup", "nepomukbackup"},
     {"Nepomuk Cleaner", "nepomukcleaner"},
     {"Oracle VM VirtualBox", "VirtualBox ", "/usr/share/icons/hicolor/16x16/mimetypes/virtualbox.png" },
     {"QSystemd", "qsystemd"},
     {"QTerminal", "qterminal"},
     {"QTerminal drop down", "qterminal --drop", "/usr/share/pixmaps/qterminal.png" },
     {"Terminator", "terminator", "/usr/share/icons/hicolor/16x16/apps/terminator.png" },
     {"Termite", "termite"},
     {"TrueCrypt", "truecrypt", "/usr/share/pixmaps/truecrypt.xpm" },
     {"UNetbootin", "/usr/bin/unetbootin_polkit", "/usr/share/icons/hicolor/16x16/apps/unetbootin.png" },
     {"UXTerm", "uxterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
     {"Wireshark", "wireshark ", "/usr/share/icons/hicolor/16x16/apps/wireshark.png" },
     {"XTerm", "xterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
     {"Yakuake", "yakuake", "/usr/share/icons/hicolor/16x16/apps/yakuake.png" },
     {"dconf Editor", "dconf-editor", "/usr/share/icons/hicolor/16x16/apps/dconf-editor.png" },
     {"urxvt", "urxvt"},
     {"urxvt (client)", "urxvtc"},
     {"urxvt (tabbed)", "urxvt-tabbed"},
 }

xdgmenu = {
    {"Accessories", menu98edb85b00d9527ad5acebe451b3fae6},
    {"Education", menude7a22a0c94aa64ba2449e520aa20c99},
    {"Games", menu251bd8143891238ecedc306508e29017},
    {"Graphics", menud334dfcea59127bedfcdbe0a3ee7f494},
    {"Internet", menuc8205c7636e728d448c2774e6a4a944b},
    {"Office", menudf814135652a5a308fea15bff37ea284},
    {"Programming", menue6f43c40ab1c07cd29e4e83e4ef6bf85},
    {"Sound & Video", menu52dd1c847264a75f400961bfb4d1c849},
    {"System Tools", menuee69799670a33f75d45c57d1d1cd0ab3},
}

